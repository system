#!/bin/sh
BEFORE=$(du -h /opt/beauties/data/storage)
echo "#universe +++ $BEFORE - Starting cleanup ..." | nc -N 127.0.0.2 2424
find /opt/beauties/data/storage/* -mtime +60 -exec rm {} \; | nc -N 127.0.0.2 2424
AFTER=$(du -h /opt/beauties/data/storage)
echo "#universe +++ $AFTER - Cleanup complete." | nc -N 127.0.0.2 2424
