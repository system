#!/bin/sh
#Simple wrapper for shmux
#https://github.com/shmux/shmux

HOSTS=`grep -w Host .ssh/config | sed -e 's/Host / /' - | sed -e 's/#.*//' - | tr -d '\n'`
PAYLOAD="curl -s 202.61.255.170:9090/scripts/$1 | sh -"

/usr/local/bin/shmux -C 5s -a lnregex -o /tmp/multi -r ssh -S all -T 10 -m -v -p -c "$PAYLOAD" $HOSTS
