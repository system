#!/bin/sh
DISTRIB=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
echo $DISTRIB
if [[ ${DISTRIB} = "openSUSE Leap" ]]
then
read -p "Deploy Restic on this system? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
zypper in -y restic libcap-progs
useradd -rUd /opt/restic -s /bin/false restic
mkdir /opt/restic
mkdir /etc/restic
chown restic:restic /opt/restic
chown restic:restic /etc/restic
chmod 700 /etc/restic
chmod 700 /opt/restic
setcap cap_dac_read_search=+ep /usr/bin/restic
cat <<'EOF' >/opt/restic/run.sh
#!/bin/bash
#/usr/bin/echo Executing Restic S3 Backup for $(hostname -f)
export RESTIC_REPOSITORY="s3:$S3PROTO://$S3HOST/$S3BUCKET"
export RESTIC_PASSWORD_FILE="$LIBPASS"
export AWS_ACCESS_KEY_ID="$S3AKI"
export AWS_SECRET_ACCESS_KEY="$S3SAK"
export RESTIC_CACHE_DIR="$CACHE"
EXCLUDE=".restic.excludes"
/usr/bin/restic --verbose backup --exclude-file="/etc/$EXCLUDE" /etc
echo "EXIT $?"
/usr/bin/restic --verbose backup --exclude-file="/home/$EXCLUDE" /home
echo "EXIT $?"
/usr/bin/restic --verbose backup --exclude-file="/opt/$EXCLUDE" /opt
echo "EXIT $?"
/usr/bin/restic --verbose backup --exclude-file="/srv/$EXCLUDE" /srv
echo "EXIT $?"
EOF
chown restic:restic /opt/restic/run.sh
chmod 700 /opt/restic/run.sh
touch /etc/.restic.excludes
cat <<'EOF' >/home/.restic.excludes
.snapshots
georg/stuff
EOF
cat <<'EOF' >/opt/.restic.excludes
.snapshots
node_exporter*
restic
EOF
cat <<'EOF' >/srv/.restic.excludes
.snapshots
EOF
cat <<'EOF' >/etc/cron.d/restic
# Cronjob for Restic Backup to Wasabi S3
# Created by deploy_restic.sh
# georg@lysergic.dev

MAILTO=system
SHELL=/bin/sh

0 20 * * Sun restic /opt/restic/run.sh |& mail -s "S3 Backup - $(hostname -f) - $(date)" ircsystem
EOF
echo OK
fi
else
echo "This is currently only compatible with SUSE nodes."
fi
