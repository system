#!/bin/sh
# Floating IP switching script utilizing the Netcup API

SCP_USER="12345"
SCP_PASS="xxx"
SCP_SERVER="v6789"
MAC="xx:xx:xx:xx:xx:xx"
IP_v4="192.0.2.10/32"
IP_v6="not:tested::/64"

URL="https://www.servercontrolpanel.de/WSEndUser?xsd=1" ### ?xsd=1 ?wsdl

usage () {
        echo "$0 [--ipv4 | --ipv6 | --all] [--debug]"
        exit 2
}

init () {
        construct "$1"
        run
        parse
}

construct () {
        if [ "$1" = "ip4" ];
        then
                local IP="$IP_v4"
        fi
        if [ "$1" = "ip6" ];
        then
                local IP="$IP_v6"
        fi
        local CIDR="${IP#*/}"
        local IP="`echo $IP | sed "s?/$CIDR??"`"
        if [ "$DEBUG" = "true" ];
        then
                echo "[DEBUG] Initiating: $1"
                echo "[DEBUG] IP Address: $IP"
                echo "[DEBUG] CIDR Mask: $CIDR"
        fi
        XML_BODY="<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='http://enduser.service.web.vcp.netcup.de/'><SOAP-ENV:Body><ns1:changeIPRouting><loginName>$SCP_USER</loginName><password>$SCP_PASS</password><routedIP>$IP</routedIP><routedMask>$CIDR</routedMask><destinationVserverName>$SCP_SERVER</destinationVserverName><destinationInterfaceMAC>$MAC</destinationInterfaceMAC></ns1:changeIPRouting></SOAP-ENV:Body></SOAP-ENV:Envelope>"
        if [ "$DEBUG" = "true" ];
        then
                echo "[DEBUG] Payload: $XML_BODY"
        fi
}

request () {
        curl -s -H 'Content-Type: text/xml' -H 'SOAPAction:' -d "$XML_BODY" -X POST "$URL"
}

run () {
        RESPONSE=`request`
        if [ "$DEBUG" = "true" ];
        then
                echo "[DEBUG] Response: $RESPONSE"
        fi

}

parse () {
        local IFS='&'
        local check_invalid="validation error&IP already assigned&true"
        for check in $check_invalid;
        do
                if [ "$DEBUG" = "true" ];
                then
                        echo "[DEBUG] Parsing: $check"
                fi
                if [ "${RESPONSE#*$check}" = "$RESPONSE" ];
                then
                        result="Not found"
                fi
                if [ "${RESPONSE#*$check}" != "$RESPONSE" ];
                then
                        result="Found"
                fi
                echo "Check for \"$check\": $result"
        done
}

MODE="$1"

if [ "$2" = "--debug" ];
then
        DEBUG="true"
        echo "[DEBUG] Script invoked at `date`"
fi

case "$MODE" in
        "--ipv4" )
                init ip4
                ;;
        "--ipv6" )
                init ip6
                ;;
        "--all" )
                init ip6
                init ip4
                ;;
        * )
                usage
                ;;
esac

