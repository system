#!/bin/sh
if [ ! -d /opt/ergo ]
echo
then
read -p "Remove Ergo? This will kill running Ergo services and cause data loss. " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo "Removing Ergo"
systemctl disable --now ergo.service || true
rm -f /etc/systemd/system/ergo.service
rm -rf /opt/ergo
userdel -f ergo
groupdel -f ergo
echo "OK"
fi
else
echo "Could not find a compatible installation of Ergo."
fi
