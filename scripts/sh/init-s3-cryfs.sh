#!/bin/sh
#Georg Pfuetzenreuter <georg@lysergic.dev>
#Not intended for general purpose use - designed to initialize a construct of bind -> CryFS -> S3 mounts

basemount="/s3"
cryptmount="$basemount/container"
sysmount="/mnt/s3cry"
datamount="/data"
ingestmount="$datamount/ingest/s3"
clearingestmount="${ingestmount}_clear"

mounts="/proc/mounts"

check () {
	grep -qs "$1" "$mounts"
}

check_ingestmount () {
	if check "$ingestmount"
	then
		echo "Already mounted."
		exit 0
	fi
}

stop_mounts () {
	if check "$ingestmount "
	then
		echo "Unmounting $ingestmount ..."
		umount "$ingestmount"
	fi
	if check "$clearingestmount "
	then
		echo "Unmounting $clearingestmount ..."
		umount "$clearingestmount"
	fi
	if check "$sysmount"
	then
		echo "Unmounting $sysmount ..."
		fusermount -u "$sysmount"
	fi
	if check "$basemount"
	then
		echo "Unmounting $basemount ..."
		umount "$basemount"
	fi
}

start_mounts () {
	check_ingestmount
	if ! check "$basemount"
	then
		echo "Mounting $basemount ..."
		s3fs lysergic "$basemount" -o url=https://s3.eu-central-2.wasabisys.com -o allow_other
		if [ ! "$?" = "0" ]
		then
			echo "FATAL - s3fs failed"
			exit 1
		fi

		#we don't want this, it's an unencrypted s3 mount, but helpful for testing
		if ! check "$clearingestmount "
		then
			echo "Mounting $clearingestmount ..."
			mount -o bind "$basemount/clear" "$clearingestmount"
		fi

		if ! check "$sysmount"
		then
			echo "Mounting $sysmount ..."
			cryfs "$cryptmount" "$sysmount" -- -o allow_other < /etc/.cry >/dev/null 2>&1
			if [ ! "$?" = "0" ]
			then
				echo "FATAL - cryfs failed"
				exit 1
			fi
			if ! check "$ingestmount "
			then
				echo "Mounting $ingestmount ..."
				mount -o bind "$sysmount" "$ingestmount"
				if [ ! "$?" = "0" ]
				then
					echo "FATAL - bind mount failed"
					exit 1
				fi
			fi
		fi
	fi
}

case "$1" in
	"start" ) start_mounts ;;
	"stop" ) stop_mounts ;;
	"status" ) check_ingestmount ;;
	* ) echo "$0 [start|stop|status]" ;;
esac
