#!/bin/sh
# Query tor node
# Designed to be run with multi.sh

version_check() {
        if [ -f "$PKGBINARY" ]; then
                echo "Packaged version:"
                $PKGBINARY --version
        else
                echo "No packaged version."
        fi
        if [ -f "$SRCBINARY" ]; then
                echo "Source version:"
                $SRCBINARY --version
        else
                echo "No source version."
        fi

}

run_check_linux() {
        # to-do: check if Sytemd or SysV-Init and behave accordingly
        systemctl is-active tor
}

run_check_bsd() {
        if [ -f "$RCSCRIPT" ]; then
                $RCSCRIPT status
        else
                ps aux |grep tor
        fi
}

echo "Found in PATH: `command -v tor`"

case `uname` in
        'Linux' )
                PKGBINARY="/usr/bin/tor"
                SRCBINARY="/usr/local/bin/tor"
                version_check
                run_check_linux
        ;;
        'NetBSD' )
                PKGBINARY="/usr/pkg/bin/tor"
                SRCBINARY="/usr/local/bin/tor"
                RCSCRIPT="/etc/rc.d/tor"
                version_check
                run_check_bsd
        ;;
esac
