#!/bin/sh
DISTRIB=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
echo "Detected $DISTRIB"
if [[ ${DISTRIB} = "openSUSE Leap" ]]
echo
then
if [ ! -d /opt/ergo ]
then
GOVER=`go version | { read _ _ v _; echo ${v#go}; }`
echo "Detected Go $GOVER"
GOVER_REQ="1.16.0"
if [ "$(printf '%s\n' "$GOVER_REQ" "$GOVER" | sort -V | head -n1)" = "$GOVER_REQ" ]
then
read -p "Deploy Ergo on this system? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
zypper in --no-recommends -y git make
REPO="https://github.com/ergochat/ergo.git"
mkdir /opt/ergo
useradd -rUd /opt/ergo -s /bin/false ergo
chown ergo:ergo /opt/ergo
chmod 750 /opt/ergo
usermod -aG ergo pratyush
usermod -aG ergo georg
sudo -u ergo git clone $REPO /opt/ergo/ergo-git
cd /opt/ergo/ergo-git
sudo -u ergo make build
sudo -u ergo chmod +x ergo
ln -s /opt/ergo/ergo-git/ergo /opt/ergo/ergo
cat <<'EOF' >/etc/systemd/system/ergo.service
[Unit]
Description=Ergo IRCd
After=network.target

[Service]
Type=simple
User=ergo
Group=ergo
ExecStart=/opt/ergo/ergo run --conf /opt/ergo/ircd.yaml
ExecReload=/bin/kill -HUP $MAINPID
Restart=on-failure
LimitNOFILE=1048576
SyslogIdentifier=ergo

[Install]
WantedBy=multi-user.target
EOF
sudo -u ergo cp /opt/ergo/ergo-git/default.yaml /opt/ergo/ircd.yaml
sudo -u ergo mkdir /opt/ergo/tls
sed -i 's/path: ircd.db/path: \/opt\/ergo\/ircd.db/' /opt/ergo/ircd.yaml
sed -i 's/cert: fullchain.pem/cert: \/opt\/ergo\/tls\/fullchain.pem/' /opt/ergo/ircd.yaml
sed -i 's/key: privkey.pem/key: \/opt\/ergo\/tls\/privkey.pem/' /opt/ergo/ircd.yaml
sed -i 's/path: languages/path: \/opt\/ergo\/ergo-git\/languages/' /opt/ergo/ircd.yaml
sudo -u ergo /opt/ergo/ergo initdb --conf /opt/ergo/ircd.yaml
sudo -u ergo /opt/ergo/ergo mkcerts --conf /opt/ergo/ircd.yaml
systemctl enable ergo.service
systemctl start ergo.service
fi
else
echo "This is currently only compatible with Go 1.16.6 or higher. Consider deploy_go.sh."
fi
else
echo "Existing Ergo installation or leftovers detected. Consider remove_ergo.sh."
fi
else
echo "This is currently only compatible with SUSE Leap nodes."
fi
