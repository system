#!/bin/sh
DISTRIB=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
echo $DISTRIB
if [[ ${DISTRIB} = "openSUSE Leap" ]]
echo
then
read -p "Deploy Go on this system? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
if [ ! -f /etc/zypp/repos.d/go.repo ]
then
REPO="https://download.opensuse.org/repositories/devel:/languages:/go/openSUSE_Leap_15.3/devel:languages:go.repo"
zypper ar --gpgcheck-allow-unsigned-repo $REPO
fi
zypper ref
zypper in -y go
echo OK
fi
else
echo "This is currently only compatible with SUSE Leap nodes."
fi
