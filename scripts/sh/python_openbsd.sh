#!/bin/sh
# Boostrap Python 3.10 on OpenBSD 7.1
# Run on management host

target="$1"
share="/mnt/storage-warsaw/shared/openbsd/python/"
tempdir="/tmp"
packages="bzip2-1.0.8p0.tgz libffi-3.3p1.tgz xz-5.2.5p0.tgz sqlite3-3.38.2.tgz libiconv-1.16p0.tgz gettext-runtime-0.21p1.tgz python-3.10.4p0.tgz"

install () {
	local package="$tempdir/$1"
	ssh -t "$target" "doas pkg_install $package"
}

if [ -d "$share" ];
then
	echo "Is the deployment share not mounted?"
	exit 1
fi

if [ -z "$target" ];
then
	echo "Specify a target host you have access to."
	exit 1
fi

if [ ! "ssh -q $target hostname" ];
then
	echo "Unable to connect to host, aborting."
	exit 1
fi

scp -C "$share/*.tgz" "$target:$tempdir/"

for package in "$packages";
do
	install "$package"
done
