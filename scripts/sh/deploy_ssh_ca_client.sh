#!/bin/sh
#
# Deploys SSH client configuration for nodes with CA signed host certificates and CA based user authentication. Standalone nodes may not use this script.
# Currently only designed for systemd based GNU/Linux distributions and OpenBSD. To-Do: support Sys-V init and Lukem RC based systems.
#
# Author: Georg Pfuetzenreuter <georg@lysergic.dev>
# Last edit: 04/12/2021
#
# Tested on OpenBSD and OpenSUSE.

get_ip_address () {
        case $KERNEL in
                "OpenBSD" ) ifconfig  | grep -E 'inet.[0-9]' | grep -v '127.0.0.1' | awk '{ print $2}' | head -n1
                ;;
                "Linux" ) ip addr show eth0 | awk '$1 == "inet" {gsub(/\/.*$/, "", $2); print $2}'
                ;;
        esac

}
HOSTNAME=$(hostname -s)
KERNEL=$(uname)
IP_ADDRESS="$(get_ip_address)"
if [ "$KERNEL" = "OpenBSD" ] || [ "$KERNEL" = "Linux" ]; then
        if [ -f /tmp/$HOSTNAME ] && [ -f /tmp/$HOSTNAME-cert.pub ]; then
                if [ ! -d /etc/ssh/old ]; then
                        mkdir /etc/ssh/old
                fi
                if [ -f /etc/ssh/ssh_known_hosts ]; then
                        mv /etc/ssh/ssh_known_hosts /etc/ssh/old/
                fi
                #if compgen -G "/etc/ssh/ssh_host_*" > /dev/null; then
                #mv /etc/ssh/ssh_host_* /etc/ssh/old/
                #fi
                if [ -f /etc/ssh/ssh_host_rsa_key ]; then
                        mv /etc/ssh/ssh_host_* /etc/ssh/old/
                fi
                mv /etc/ssh/sshd_config /etc/ssh/old/
                if [ -f /etc/ssh/ssh_config ]; then
                        mv /etc/ssh/ssh_config /etc/ssh/old/
                fi
                mv /tmp/$HOSTNAME /etc/ssh/
                mv /tmp/$HOSTNAME-cert.pub /etc/ssh/
                cat <<'EOF_SSHD_CONFIG' >/etc/ssh/sshd_config
ListenAddress   %%IP_ADDRESS%%
Protocol        2
SyslogFacility  AUTH
LogLevel        FATAL

HostKey                         /etc/ssh/%%HOSTNAME%%
HostCertificate                 /etc/ssh/%%HOSTNAME%%-cert.pub
TrustedUserCAKeys               /etc/ssh/user_ca
PasswordAuthentication          no
ChallengeResponseAuthentication no
AuthenticationMethods           publickey

LoginGraceTime  1m
PermitRootLogin no
StrictModes     yes
MaxAuthTries    1
MaxSessions     3

X11Forwarding   no
PrintMotd       yes
PrintLastLog    yes
EOF_SSHD_CONFIG
        sed -i -e "s/%%IP_ADDRESS%%/$IP_ADDRESS/" -e "s/%%HOSTNAME%%/$HOSTNAME/" /etc/ssh/sshd_config
        cat <<'EOF_USER_CA' >/etc/ssh/user_ca
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOLbLqHWXcxLGf58aJwa4eSC3KYGfdIiluKynOXS/fZD root@philia.rigel.lysergic.dev
EOF_USER_CA
                case $KERNEL in
                        "OpenBSD" ) rcctl reload sshd
                        ;;
                        "Linux" ) systemctl reload sshd
                        ;;
                esac
                echo "OK"
        else
                echo "Missing host certificate and public key, copy them to /tmp/ for me."
        fi
else
        echo "Unsupported operating system, please configure sshd manually."
fi
