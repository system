#!/bin/sh
DISTRIB=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
echo "Detected $DISTRIB"
if [ ${DISTRIB} = "openSUSE Leap" ]
echo
then
if [ ! -d /opt/app ]
then
echo "app not found. Preparing Install"

# Distrib package manager installs deps
zypper in --no-recommends -y git make
REPO="<repo_url.git>"
mkdir /opt/app
useradd -rUd /opt/app -s /bin/false app
chown app:app /opt/app
chmod 750 /opt/app
usermod -aG app pratyush
usermod -aG app georg
sudo -u app git clone $REPO /opt/app/app-git
cd /opt/app/app-git || exit
sudo -u app make build
sudo -u app chmod +x app
ln -s /opt/app/app-git/app /opt/app/app

# Insert application specific commands here.

# consider a change in directory structure based on applications?
# update the statement below accordingly.
sudo cp path/to/unitfile /etc/systemd/system/app.service
sudo systemctl daemon-reload
sudo systemctl enable --now app.service
else
echo "Existing app installation or leftovers detected. Consider remove_app.sh."
fi
else
echo "This is currently only compatible with SUSE Leap nodes."
fi