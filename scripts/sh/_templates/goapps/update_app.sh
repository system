#!/bin/sh

# Only supports git source builds currently.

if [ -f /opt/app/app-git/app ]
then
echo "This will immediately stop app if it is running. Update app on this system? "
read text -r
expr "$text" : "^[Yy]$" > /dev/null
if [ "$text" ] 
then
# Inconsistent `sudo` use?
systemctl stop app.service

# Update golang using `deploy_go`

cd /opt/app/app-git || exit 
rm -f app
git pull
sudo -u app make build
# insert misc commands as needed.
systemctl start app.service
fi
else
echo "app does not seem to be installed. Consider deploy_app.sh."
fi