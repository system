# custom
function __ssh_agent_is_key_added -d "checks if fingerprint is already present"
        set SSH_FP (ssh-keygen -lf ~/.ssh/id_lysergic-cert.pub | awk '{print $2}')
        if ! test -z "$SSH_FP"
                and ssh-add -l | grep -q "$SSH_FP"
        else
                return 1
        end
end
