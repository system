# original from https://github.com/danhper/fish-ssh-agent/
# adapted to include ssh-add check
function sshagent
        if test -z "$SSH_ENV"
            set -xg SSH_ENV $HOME/.ssh/environment
        end

        if not __ssh_agent_is_started
            __ssh_agent_start
        end

        if not __ssh_agent_is_key_added
                echo "You need to authenticate."
                ssh-add -q ~/.ssh/id_lysergic
        end
end
